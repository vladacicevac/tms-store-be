<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(["namespace"=>"AuthApi", "middleware"=>"auth:api"], function () {

    Route::post('/authenticate/logout', [
        'as' => 'login',
        'uses'=>'LoginController@logout'
    ]);

});

Route::group(["middleware"=>"auth:api"], function () {


    Route::get('/callbacks', [
        'uses'=>'CallbackFormController@index'
    ]);

    Route::delete('/callbacks/{id}', [
        'uses'=>'CallbackFormController@delete'
    ]);

    Route::get('/callbacks/{id}', [
        'uses'=>'CallbackFormController@find'
    ]);

    Route::post('/callbacks/{id}/add-comment', [
        'uses'=>'CallbackFormController@addComment'
    ]);

});


Route::group(["namespace"=>"AuthApi"], function () {

    Route::post('/authenticate/login', [
        'as' => 'login',
        'uses'=>'LoginController@login'
    ]);

    Route::post('/authenticate/login/refresh', [
        'uses'=>'LoginController@refreshToken'
    ]);
});



Route::post('/callbacks', [
    'uses'=>'CallbackFormController@store'
]);


