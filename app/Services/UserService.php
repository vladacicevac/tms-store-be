<?php


namespace App\Services;


use App\Contracts\Services\UserServiceInterface;
use App\Contracts\Repositories\UserRepositoryInterface;

class UserService implements UserServiceInterface
{
    protected $userRepository;



    public function __construct(UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;

    }

    public function findByEmail($email)
    {
        return $this->userRepository->findByEmail($email);
    }




    protected function createToken($user){

        $token = app(PasswordBroker::class)->createToken($user);

        return $token;
    }


}
