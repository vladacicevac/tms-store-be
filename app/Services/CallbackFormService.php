<?php


namespace App\Services;


use App\Contracts\Repositories\CallbackFormRepositoryInterface;
use App\Contracts\Services\CallbackFormServiceInterface;
use Illuminate\Support\Facades\DB;

class CallbackFormService implements CallbackFormServiceInterface
{
    protected $callbackFormRepository;

    public function __construct(CallbackFormRepositoryInterface $callbackFormRepository)
    {
        $this->callbackFormRepository = $callbackFormRepository;
    }

    public function all()
    {
        return $this->callbackFormRepository->all();
    }

    public function create($request)
    {
        return $this->callbackFormRepository->create($request);
    }

    public function find($id)
    {
        return $this->callbackFormRepository->find($id, ['comments']);
    }

    public function delete($id)
    {
        try{
            DB::beginTransaction();
            $callback = $this->callbackFormRepository->find($id);

            $this->callbackFormRepository->delete($callback->id);

            DB::commit();


        }catch(ModelNotFoundException $e){
            DB::rollback();
            throw $e;

        }catch(Exception $e){
            DB::rollback();
            throw $e;

        }

    }
}
