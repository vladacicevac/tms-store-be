<?php


namespace App\Services;


use App\Contracts\Services\UserServiceInterface;
use App\Extensions\GuzzleHttp\AdapterInterface;
use Illuminate\Foundation\Application;
use App\Exceptions\InvalidCredentialsException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;



class LoginProxy {

    const REFRESH_TOKEN = 'refreshToken';

    private $http;

    private $auth;

    private $cookie;

    private $db;

    private $request;

    private $userService;

    public function __construct(Application $app, UserServiceInterface $userService)
    {
        $this->userService = $userService;
        //$this->http = $httpClient;
        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     */
    public function attemptLogin($email, $password)
    {
        $user = $this->userService->findByEmail($email);

        $data = [
            'username' => $email,
            'password' => $password
        ];

        if (!is_null($user)) {
            return $this->proxy('password', $user, $data);
        }

        throw new InvalidCredentialsException();
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     */
    public function proxy($grantType, $user,  array $data = [])
    {

        $data = array_merge($data, [
            'client_id'     => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'grant_type'    => $grantType,
            'scope' => '*',

        ]);

        $client = new Client([
            'base_uri' => env('APP_URL')
        ]);


        try{

            $response = $client->post('oauth/token', [
//            'debug' => true,
                'form_params' => $data,
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ]
            ]);

        }catch(\GuzzleHttp\Exception\ClientException $e){

            if($e->getResponse()->getStatusCode() == 401){
                throw new InvalidCredentialsException();
            }


        }



        $body = @$response->getBody();

        //dd($response->getStatusCode());
        if ($response->getStatusCode() != 200) {
            throw new InvalidCredentialsException();
        }


        $data = json_decode((string) $body);


        $data->user = $user;
        // Create a refresh token cookie
        $this->cookie->queue(
            self::REFRESH_TOKEN,
            $data->refresh_token,
            864000, // 10 days
            null,
            null,
            false,
            true // HttpOnly
        );

        return $data;
    }

    /**
     * Attempt to refresh the access token used a refresh token that
     * has been saved in a cookie
     */
    public function attemptRefresh($data = [])
    {

        try{
            $token = $this->proxy('refresh_token', [], $data);

            return $token;
        } catch (\GuzzleHttp\Exception\ClientException $e){
            throw $e;
        }

    }

    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        //dd($accessToken);

        $refreshToken = $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }
}
