<?php

namespace App\Providers;

use App\Contracts\Repositories\CallbackFormRepositoryInterface;
use App\Contracts\Repositories\UserRepositoryInterface;
use App\Contracts\Services\CallbackFormServiceInterface;
use App\Contracts\Services\UserServiceInterface;
use App\Repositories\CallbackFormRepository;
use App\Repositories\UserRepository;
use App\Services\CallbackFormService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CallbackFormRepositoryInterface::class,
            CallbackFormRepository::class
        );
        $this->app->bind(
            CallbackFormServiceInterface::class,
            CallbackFormService::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
        $this->app->bind(
            UserServiceInterface::class,
            UserService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
