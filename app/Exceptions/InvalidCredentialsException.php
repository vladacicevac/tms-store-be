<?php


namespace App\Exceptions;


class InvalidCredentialsException extends \Exception{
    public function errorMessage()
    {
        return 'Username or password is not correct.';
    }
}
