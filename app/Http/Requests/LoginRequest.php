<?php


namespace App\Http\Requests;



class LoginRequest extends Request{


    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username'    => 'required|email',
            'password' => 'required'
        ];
    }


}
