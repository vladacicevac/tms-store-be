<?php


namespace App\Http\Requests\CallBackForm;


use App\Http\Requests\Request;

class CreateRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'gender' => 'required',
            'phone' => 'required',
            'description' => 'required',
            'time_for_callback' => 'required'
        ];
    }

    public function sanitize()
    {
        $input = $this->all();

        $input['time_for_callback'] = date('Y-m-d h:m', strtotime($this->get('time_for_callback'))) ;

        $this->replace($input);

        return $this->all();
    }

}
