<?php


namespace App\Http\Controllers;


class BaseController extends Controller
{
    /**
     * Returns and formats the responses which return sort of result (all except Errors)
     *
     * @param mixed $data
     * @param string $response_message
     * @param integer $status_code
     * @return Illuminate\Http\JsonResponse
     */
    public function response($data, $response_message, $status_code = 200)
    {

        $data = [
            'data' => $data,
            'response_message' => $response_message
        ];

        return response()->json($data, $status_code, [], JSON_NUMERIC_CHECK);
    }


    /**
     * Returns and formats responses which return error
     *
     * @param string/array $errors
     * @param integer $error_code
     * @param integer $status_code
     * @return Illuminate\Http\JsonResponse
     */
    public function error($errors, $status_code = 400)
    {
        if (is_string($errors)) {
            $errors = [$errors];
        }

        $message = [
            'status' => $status_code,
            'errors' => $errors,
        ];

        return response()->json($message, $status_code, [], JSON_NUMERIC_CHECK);
    }


    /**
     * Calls Error function with status code depending of error which was caused
     *
     * @param \Exception $e
     * @return type
     */
    public function systemError(\Exception $e)
    {
        $status = 500;
        $message = $e->getMessage();
        $type = last(explode('\\', get_class($e)));
        return $this->error(['type' => $type, 'message' => $message], $status);
    }

}
