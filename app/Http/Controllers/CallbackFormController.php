<?php


namespace App\Http\Controllers;


use App\Contracts\Services\CallbackFormServiceInterface;
use App\Http\Requests\CallBackForm\CreateRequest;
use App\Http\Requests\Request;
use App\Models\Comment;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CallbackFormController extends BaseController
{
    protected $callbackFormService;

    public function __construct(CallbackFormServiceInterface $callbackFormService)
    {
        $this->callbackFormService = $callbackFormService;
    }

    public function index()
    {
        $call = $this->callbackFormService->all();

        return $this->response($call, 'ok');
    }

    public function store(CreateRequest $request)
    {

        try{
            $this->callbackFormService->create($request);

            return $this->response([], "Success", 200);
        }catch (ModelNotFoundException $e){
            return $this->response([], "Model not found", 404);
        }
        catch (\Exception $e){
            return $this->systemError($e);
        }
    }

    public function addComment(\App\Http\Requests\Comments\CreateRequest $request, $id)
    {

        try{
            $comment = new Comment(['comment' => $request->get('comment')]);
            $form = $this->callbackFormService->find($id);

            $form->comments()->save($comment);

            $form->load('comments');

            return $this->response($form, "Success", 200);
        }catch (ModelNotFoundException $e){
            return $this->response([], "Model not found", 404);
        }
        catch (\Exception $e){
            return $this->systemError($e);
        }
    }

    public function find($id)
    {

        try{
            $form = $this->callbackFormService->find($id);

            return $this->response($form, "Success", 200);
        }catch (ModelNotFoundException $e){
            return $this->response([], "Model not found", 404);
        }
        catch (\Exception $e){
            return $this->systemError($e);
        }
    }

    public function delete($id)
    {
        //$this->callbackFormService->delete($id);

        try{
            $callback = $this->callbackFormService->delete($id);

            return $this->response([], 'Ok', 200);
        }catch (ModelNotFoundException $e){

            return $this->response([], 'Not found.', 404);
        }catch (\Exception $e){
            return $this->systemError($e);
        }

    }
}
