<?php


namespace App\Http\Controllers\AuthApi;
use App\Http\Controllers\BaseController;
use App\Services\LoginProxy;
use Illuminate\Http\Request;
use App\Requests\RefreshTokenRequest;
use App\Exceptions\InvalidCredentialsException;
Use App\Http\Requests\LoginRequest;


class LoginController extends BaseController{


    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }


    public function login(LoginRequest $request){


        $email = $request->get('username');
        $password = $request->get('password');

        try{
            $data = $this->loginProxy->attemptLogin($email, $password);
            return response()->json($data);
        }catch (InvalidCredentialsException $e){
            return $this->response([], $e->errorMessage(), 401);
        }

    }


    public function refreshToken(RefreshTokenRequest $request)
    {


        try{
            return response()->json($this->loginProxy->attemptRefresh($request->all()));
        }catch (InvalidCredentialsException $e){
            return $this->notOk([], $e->errorMessage());
        } catch (\GuzzleHttp\Exception\ClientException $e){
            return $this->response([], $e->getResponse()->getBody()->getContents(), 401);
        }


    }


    public function logout()
    {

        $this->loginProxy->logout();


    }


}
