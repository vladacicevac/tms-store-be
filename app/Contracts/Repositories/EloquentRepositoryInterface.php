<?php


namespace App\Contracts\Repositories;


interface EloquentRepositoryInterface
{
    public function all($relationships = [], $columns = ['*'], $order = []);
    public function find($id, $relationships = []);
}
