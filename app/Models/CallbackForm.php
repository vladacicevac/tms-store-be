<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallbackForm extends Model
{
    use SoftDeletes;

    public $table = 'callback_form';

    protected $fillable = [
        'name',
        'gender',
        'email',
        'phone',
        'order_no',
        'description',
        'time_for_callback'
    ];

    public function comments(){
        return $this->hasMany(Comment::class, 'callback_form_id', 'id');
    }
}
