<?php


namespace App\Repositories;


use App\Contracts\Repositories\UserRepositoryInterface;
use App\User;
use App\Http\Requests\Request;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $user) {
        $this->model = $user;
    }




    public function findByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }
}
