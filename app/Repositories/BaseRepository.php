<?php


namespace App\Repositories;

use App\Contracts\Repositories\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\Request;


abstract class BaseRepository implements EloquentRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getModel(){
        return $this->model;
    }

    public function create(Request $request)
    {

        return $this->model->create($request->all());

    }

    public function update(Model $model, $data = [])
    {
        $model->fill($data);

        $model->save();

        return $model;

    }

    public function delete($id)
    {
        $this->model->destroy($id);
    }

    public function all($relationships = [], $columns = ['*'], $order = ['created_at', 'desc'])
    {

        $result = $this->model->select($columns)->orderBy($order[0], $order[1])->get();


        $result->load($relationships);

        return $result;
    }

    public function find($id, $relationships = [])
    {
        $result = $this->model->find($id);

        if(is_null($result)){
            throw new ModelNotFoundException("Model not found.");
        }

        return $result->load($relationships);
    }
}

