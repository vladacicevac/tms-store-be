<?php


namespace App\Repositories;


use App\Contracts\Repositories\CallbackFormRepositoryInterface;
use App\Models\CallbackForm;

class CallbackFormRepository extends BaseRepository implements CallbackFormRepositoryInterface
{
    //protected $model;

    public function __construct(CallbackForm $collbackForm)
    {
        $this->model = $collbackForm;
    }


}
